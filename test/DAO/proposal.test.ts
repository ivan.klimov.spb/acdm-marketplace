const { expect } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { DAO, ERC20Token, ERC20Token__factory } from "../../src/types";
import { deploymentXXXToken, MINT_XXX_TOKEN } from "../XXXToken/deployXXXToken";
import {
  chairPersonId,
  debatingPeriodDuration,
  deployDAO,
} from "./deploymentDAO";
import { getTimestamp } from "../getTimestamp";

describe("Proposal ", function () {
  let dao: DAO;
  let accounts: SignerWithAddress[];
  let chairPerson: SignerWithAddress;
  let voteToken: ERC20Token;
  let jsonAbi;
  let calldata: string;
  let recipient: string;

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    chairPerson = accounts[chairPersonId];

    voteToken = await deploymentXXXToken(); //not important which erc20

    dao = await deployDAO();

    await voteToken.grantRole(await voteToken.MINTER_ROLE(), dao.address);

    calldata = ERC20Token__factory.createInterface().encodeFunctionData(
      "mint",
      [accounts[1].address, MINT_XXX_TOKEN]
    );
    recipient = voteToken.address;
  });

  it("Should added proposal and correct calldata", async function () {
    await dao.connect(chairPerson).addProposal(calldata, recipient);

    var proposal = await dao.proposal(1);

    expect(proposal[5]).to.equal(calldata);
  });

  it("Should added proposal and correct recipient", async function () {
    await dao.connect(chairPerson).addProposal(calldata, recipient);

    var proposal = await dao.proposal(1);

    expect(proposal[4]).to.equal(recipient);
  });

  it("Should added proposal and correct endDate", async function () {
    await dao.connect(chairPerson).addProposal(calldata, recipient);

    var proposal = await dao.proposal(1);
    let timeStamp = await getTimestamp();

    expect(proposal[0]).to.equal(timeStamp + debatingPeriodDuration);
  });

  it("Should proposal length has increased", async function () {
    const proposalsLengthBeforeAdding = await dao.proposalsLength();

    await dao.connect(chairPerson).addProposal(calldata, recipient);

    const proposalsLengthAfterAdding = await dao.proposalsLength();

    expect(
      proposalsLengthAfterAdding.toNumber() -
        proposalsLengthBeforeAdding.toNumber()
    ).to.equal(1);
  });

  describe("Reverted", function () {
    it("Only chair person can add proposal", async () => {
      await expect(
        dao.connect(accounts[1]).addProposal(calldata, recipient)
      ).to.be.revertedWith("DAO: Only chair person");
    });

    it("can't get 0 proposal has not been created", async () => {
      await expect(dao.proposal(0)).to.be.revertedWith(
        "DAO:proposal hasn't been created"
      );
    });

    it("can't get proposal has not been created", async () => {
      await dao.connect(chairPerson).addProposal(calldata, recipient);
      await expect(dao.proposal(2)).to.be.revertedWith(
        "DAO:proposal hasn't been created"
      );
    });
  });
});
