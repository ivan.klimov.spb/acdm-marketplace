const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { DAO, ERC20Token, IERC20, Staking } from "../../src/types";
import { BigNumber } from "ethers";
import {
  chairPersonId,
  debatingPeriodDuration,
  deployDAO,
  minimumQuorum,
} from "./deploymentDAO";

describe("Deployment DAO", function () {
  let dao: DAO;
  let accounts: SignerWithAddress[];
  let chairPerson: SignerWithAddress;

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    chairPerson = accounts[chairPersonId];

    dao = await deployDAO();
  });

  it("Should set the right chairPerson", async function () {
    expect(await dao.chairPerson()).to.equal(chairPerson.address);
  });

  it("Should set the right minimumQuorum", async function () {
    expect(await dao.minimumQuorum()).to.equal(minimumQuorum);
  });

  it("Should set the right minimumQuorum", async function () {
    expect(await dao.debatingPeriodDuration()).to.equal(debatingPeriodDuration);
  });
});
