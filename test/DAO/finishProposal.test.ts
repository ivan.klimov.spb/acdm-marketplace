const { expect, assert } = require("chai");
import { ethers, network } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { BigNumber } from "ethers";
import {
  DAO,
  ERC20Token,
  ERC20Token__factory,
  IERC20,
  Staking,
} from "../../src/types";
import { chairPersonId, deployDAO } from "./deploymentDAO";
import { deploymentXXXToken, MINT_XXX_TOKEN } from "../XXXToken/deployXXXToken";
import { initStakingForTests } from "../Staking/deploymentStaking";
import { getTimestamp } from "../getTimestamp";

const AGREE = true;
const DISAGREE = false;

describe("FinishProposal ", function () {
  let dao: DAO;
  let accounts: SignerWithAddress[];
  let chairPerson: SignerWithAddress;
  let XXXToken: ERC20Token;
  let XXXToken2: ERC20Token;
  let lpToken: IERC20;
  let calldata: string;
  let recipient: string;
  let stakingContract: Staking;

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    chairPerson = accounts[chairPersonId];

    XXXToken = await deploymentXXXToken(); //not important which erc20
    XXXToken2 = await deploymentXXXToken();

    dao = await deployDAO();

    calldata = ERC20Token__factory.createInterface().encodeFunctionData(
      "mint",
      [accounts[1].address, MINT_XXX_TOKEN]
    );

    recipient = XXXToken.address;

    stakingContract = await initStakingForTests(XXXToken, lpToken, accounts);

    await dao.connect(chairPerson).addProposal(calldata, recipient);
    await dao.connect(chairPerson).setStakingAddress(stakingContract.address);

    await XXXToken.grantRole(await XXXToken.MINTER_ROLE(), dao.address); // fo testing
  });

  it("Should finised with agree and will do proposal(account[1] get ERC20 after mint)", async function () {
    await dao.connect(accounts[1]).vote(1, AGREE);
    await dao.connect(accounts[2]).vote(1, AGREE);
    await dao.connect(accounts[3]).vote(1, DISAGREE);

    const timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + (await dao.debatingPeriodDuration()).toNumber(),
    ]);

    const balanceERC20BeforeFinish = await XXXToken.balanceOf(
      accounts[1].address
    );
    await dao.finishProposal(1);

    const balanceERC20AfterFinish = await XXXToken.balanceOf(
      accounts[1].address
    );
    expect(balanceERC20AfterFinish.sub(balanceERC20BeforeFinish)).to.deep.equal(
      MINT_XXX_TOKEN
    );
  });

  it("Should finised and status will be finished)", async function () {
    await dao.connect(accounts[1]).vote(1, AGREE);
    await dao.connect(accounts[2]).vote(1, AGREE);
    await dao.connect(accounts[3]).vote(1, DISAGREE);

    const timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + (await dao.debatingPeriodDuration()).toNumber(),
    ]);

    await dao.finishProposal(1);

    const proposal = await dao.proposal(1);

    expect(proposal.finished).to.be.true;
  });

  it("Should finised with disagree and will do nothing)", async function () {
    await dao.connect(accounts[1]).vote(1, AGREE);
    await dao.connect(accounts[2]).vote(1, DISAGREE);
    await dao.connect(accounts[3]).vote(1, DISAGREE);

    const timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + (await dao.debatingPeriodDuration()).toNumber(),
    ]);

    const balanceERC20BeforeFinish = await XXXToken.balanceOf(
      accounts[1].address
    );

    await dao.finishProposal(1);

    const balanceERC20AfterFinish = await XXXToken.balanceOf(
      accounts[1].address
    );

    expect(balanceERC20AfterFinish.sub(balanceERC20BeforeFinish)).to.deep.equal(
      0
    );
  });

  it("Should finised with not enough votes and will do nothing)", async function () {
    const timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + (await dao.debatingPeriodDuration()).toNumber(),
    ]);

    const balanceERC20BeforeFinish = await XXXToken.balanceOf(
      accounts[1].address
    );

    await dao.finishProposal(1);

    const balanceERC20AfterFinish = await XXXToken.balanceOf(
      accounts[1].address
    );

    expect(balanceERC20AfterFinish.sub(balanceERC20BeforeFinish)).to.deep.equal(
      0
    );
  });

  it("All votes finished for account[1])", async function () {
    await dao.connect(accounts[1]).vote(1, AGREE);
    const timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + (await dao.debatingPeriodDuration()).toNumber(),
    ]);

    await dao.finishProposal(1);
    await dao.allVotesFinished(accounts[1].address);

    expect(await dao.lengthVotedProposal(accounts[1].address)).to.equal(0);
  });

  it("Votes not finished for account[1])", async function () {
    await dao.connect(accounts[1]).vote(1, AGREE);
    const timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + (await dao.debatingPeriodDuration()).toNumber(),
    ]);

    await dao.allVotesFinished(accounts[1].address);

    expect(await dao.lengthVotedProposal(accounts[1].address)).to.equal(1);
  });

  describe("Reverted", function () {
    it("already finished", async () => {
      const timeStamp = await getTimestamp();
      await ethers.provider.send("evm_mine", [
        timeStamp + (await dao.debatingPeriodDuration()).toNumber(),
      ]);

      await dao.finishProposal(1);

      await expect(dao.finishProposal(1)).to.be.revertedWith(
        "DAO: already finished"
      );
    });

    it("time voting has not ended", async () => {
      await expect(dao.finishProposal(1)).to.be.revertedWith(
        "DAO: time voting has not ended"
      );
    });

    it("ERROR call function", async () => {
      await dao.connect(chairPerson).addProposal(calldata, XXXToken2.address);

      await dao.connect(accounts[1]).vote(2, AGREE);
      await dao.connect(accounts[2]).vote(2, AGREE);
      await dao.connect(accounts[3]).vote(2, DISAGREE);

      const timeStamp = await getTimestamp();
      await ethers.provider.send("evm_mine", [
        timeStamp + (await dao.debatingPeriodDuration()).toNumber(),
      ]);

      await expect(dao.finishProposal(2)).to.be.revertedWith(
        "ERROR call function"
      );
    });
  });
});
