const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { ERC20Token } from "../../src/types";
import { BigNumber } from "ethers";
import {
  DECIMALS_ACDM_TOKEN,
  deploymentACDMToken,
  NAME_ACDM_TOKEN,
  SYMBOL_ACDM_TOKEN,
} from "./deployACDMToken";

describe("Deployment ACDMToken", function () {
  let accounts: SignerWithAddress[];
  let ACDMToken: ERC20Token;

  beforeEach(async function () {
    ACDMToken = await deploymentACDMToken();
  });

  it("Should set the right name", async function () {
    expect(await ACDMToken.name()).to.equal(NAME_ACDM_TOKEN);
  });

  it("Should set the right symbol", async function () {
    expect(await ACDMToken.symbol()).to.equal(SYMBOL_ACDM_TOKEN);
  });

  it("Should set the right decimal", async function () {
    expect(await ACDMToken.decimals()).to.equal(DECIMALS_ACDM_TOKEN);
  });
});
