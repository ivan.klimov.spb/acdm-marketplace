import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { ethers } from "hardhat";
import { ERC20Token } from "../../src/types";
import { BigNumber } from "ethers";

export const NAME_ACDM_TOKEN: string = "ACADEM Coin";
export const SYMBOL_ACDM_TOKEN: string = "ACDM";
export const DECIMALS_ACDM_TOKEN: number = 6;

export async function deploymentACDMToken() {
  let accounts: SignerWithAddress[];
  let ACDMToken: ERC20Token;
  accounts = await ethers.getSigners();

  const factory = await ethers.getContractFactory("ERC20Token");
  ACDMToken = await factory.deploy(
    NAME_ACDM_TOKEN,
    SYMBOL_ACDM_TOKEN,
    DECIMALS_ACDM_TOKEN
  );
  await ACDMToken.deployed();
  return ACDMToken;
}
