const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { ERC20Token } from "../../src/types";
import { BigNumber } from "ethers";
import "./deployXXXToken";
import {
  DECIMALS_XXX_TOKEN,
  deploymentXXXToken,
  NAME_XXX_TOKEN,
  SYMBOL_XXX_TOKEN,
} from "./deployXXXToken";

describe("Deployment XXXToken", function () {
  let XXXToken: ERC20Token;

  beforeEach(async function () {
    XXXToken = await deploymentXXXToken();
  });

  it("Should set the right name", async function () {
    expect(await XXXToken.name()).to.equal(NAME_XXX_TOKEN);
  });

  it("Should set the right symbol", async function () {
    expect(await XXXToken.symbol()).to.equal(SYMBOL_XXX_TOKEN);
  });

  it("Should set the right decimal", async function () {
    expect(await XXXToken.decimals()).to.equal(DECIMALS_XXX_TOKEN);
  });
});
