import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { ethers } from "hardhat";
import { ERC20Token } from "../../src/types";
import { BigNumber } from "ethers";

export const NAME_XXX_TOKEN: string = "XXX Coin";
export const SYMBOL_XXX_TOKEN: string = "XXX";
export const DECIMALS_XXX_TOKEN: number = 18;
export const MINT_XXX_TOKEN: BigNumber = BigNumber.from(10)
  .pow(DECIMALS_XXX_TOKEN)
  .mul(10000);

export async function deploymentXXXToken() {
  let accounts: SignerWithAddress[];
  let XXXToken: ERC20Token;
  accounts = await ethers.getSigners();

  const factory = await ethers.getContractFactory("ERC20Token");
  XXXToken = await factory.deploy(
    NAME_XXX_TOKEN,
    SYMBOL_XXX_TOKEN,
    DECIMALS_XXX_TOKEN
  );
  await XXXToken.deployed();
  return XXXToken;
}
