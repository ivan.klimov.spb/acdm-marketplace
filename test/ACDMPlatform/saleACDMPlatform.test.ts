const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import {
  ACDMPlatform,
  DAO,
  ERC20Token,
  IERC20,
  Staking,
} from "../../src/types";
import { chairPersonId, deployDAO } from "../DAO/deploymentDAO";
import { deploymentXXXToken } from "../XXXToken/deployXXXToken";
import { deploymentACDMToken } from "../ACDMToken/deployACDMToken";
import {
  deploymentACDMPlatform,
  PlatformState,
  startPrice,
  uniswapRouterAddress,
} from "./deploymentACDMPlatform";
import { getTimestamp } from "../getTimestamp";
import { BigNumber } from "ethers";

describe("sale ACDMPlatform", function () {
  let dao: DAO;
  let accounts: SignerWithAddress[];
  let chairPerson: SignerWithAddress;
  let XXXToken: ERC20Token;
  let lpToken: IERC20;
  let calldata: string;
  let recipient: string;
  let stakingContract: Staking;
  let ACDMToken: ERC20Token;
  let ACDMPlatform: ACDMPlatform;

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    chairPerson = accounts[chairPersonId];

    XXXToken = await deploymentXXXToken();
    ACDMToken = await deploymentACDMToken();

    /*await XXXToken.mint(accounts[1].address, MINT_XXX_TOKEN.mul(2));
    lpToken = await addLiquidityETH(XXXToken, 1);

    stakingContract = await initStakingForTests(XXXToken, lpToken, accounts);*/

    dao = await deployDAO();

    ACDMPlatform = await deploymentACDMPlatform(XXXToken, ACDMToken, dao);

    ACDMToken.grantRole(await ACDMToken.MINTER_ROLE(), ACDMPlatform.address);
  });

  it("Should purchased half tokens on sale", async function () {
    await ACDMPlatform.connect(accounts[1]).signUpWithoutReferrer();
    await ACDMPlatform.changeState();

    await ACDMPlatform.connect(accounts[1]).buyTokensOnSale({
      value: ethers.utils.parseEther("0.5"),
    });

    expect(await ACDMPlatform.saleTokenAmount()).to.eq(
      BigNumber.from(50000000000)
    );
  });

  it("Should purchased max saleTokenAmount if value sent more than need", async function () {
    await ACDMPlatform.connect(accounts[1]).signUpWithoutReferrer();
    await ACDMPlatform.changeState();

    const saleTokenAmountBeforePurchase = await ACDMPlatform.saleTokenAmount();

    await ACDMPlatform.connect(accounts[1]).buyTokensOnSale({
      value: ethers.utils.parseEther("2"),
    });

    expect(0).to.eq(0);
  });

  it("Should transfer tokens and platform balance 90 000 ACDMTokens", async function () {
    await ACDMPlatform.connect(accounts[1]).signUpWithoutReferrer();
    await ACDMPlatform.changeState();

    await ACDMPlatform.connect(accounts[1]).buyTokensOnSale({
      value: ethers.utils.parseEther("0.1"),
    });

    const acdmTokensAfterSale = await ACDMToken.balanceOf(ACDMPlatform.address);

    expect(acdmTokensAfterSale).to.eq(90000000000);
  });

  it("Should transfer tokens and platform account balance 10 000 ACDMTokens", async function () {
    await ACDMPlatform.connect(accounts[1]).signUpWithoutReferrer();
    await ACDMPlatform.changeState();

    expect(await ACDMToken.balanceOf(accounts[1].address)).to.eq(0);

    await ACDMPlatform.connect(accounts[1]).buyTokensOnSale({
      value: ethers.utils.parseEther("0.1"),
    });

    const acdmTokensAfterSale = await ACDMToken.balanceOf(accounts[1].address);

    expect(acdmTokensAfterSale).to.eq(10000000000);
  });

  describe("Emit", function () {
    it("Emit PurchasedOnSale(address[1], saleTokenAmount: 10000 ACDM, ACDMTokenPrice, numberPlatformCircle: 1)", async () => {
      await ACDMPlatform.connect(accounts[1]).signUpWithoutReferrer();
      await ACDMPlatform.changeState();

      let price = await ACDMPlatform.ACDMTokenPrice();

      await expect(
        ACDMPlatform.connect(accounts[1]).buyTokensOnSale({
          value: ethers.utils.parseEther("0.1"),
        })
      )
        .to.emit(ACDMPlatform, "PurchasedOnSale")
        .withArgs(accounts[1].address, 10000000000, price, 1);
    });
  });

  describe("Reverted", function () {
    it("ACDMPlatform: small purchase", async () => {
      await ACDMPlatform.connect(accounts[1]).signUpWithoutReferrer();
      await ACDMPlatform.changeState();

      await expect(
        ACDMPlatform.connect(accounts[1]).buyTokensOnSale({
          value: ethers.utils.parseEther("0.0000000001"),
        })
      ).to.be.revertedWith("ACDMPlatform: small purchase");
    });

    it("ACDMPlatform: sale is over", async () => {
      await ACDMPlatform.connect(accounts[1]).signUpWithoutReferrer();

      await expect(
        ACDMPlatform.connect(accounts[1]).buyTokensOnSale({
          value: ethers.utils.parseEther("1"),
        })
      ).to.be.revertedWith("ACDMPlatform: sale is over");
    });
  });
});
