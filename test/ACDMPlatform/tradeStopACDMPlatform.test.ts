const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import {
  ACDMPlatform,
  DAO,
  ERC20Token,
  IERC20,
  Staking,
} from "../../src/types";
import { chairPersonId, deployDAO } from "../DAO/deploymentDAO";
import { deploymentXXXToken } from "../XXXToken/deployXXXToken";
import { deploymentACDMToken } from "../ACDMToken/deployACDMToken";
import {
  deploymentACDMPlatform,
  PlatformState,
  startPrice,
  uniswapRouterAddress,
} from "./deploymentACDMPlatform";
import { getTimestamp } from "../getTimestamp";
import { BigNumber } from "ethers";

const purchasedOnSale: BigNumber = BigNumber.from(50000000000);

describe("trade init ACDMPlatform", function () {
  let dao: DAO;
  let accounts: SignerWithAddress[];
  let chairPerson: SignerWithAddress;
  let XXXToken: ERC20Token;
  let lpToken: IERC20;
  let calldata: string;
  let recipient: string;
  let stakingContract: Staking;
  let ACDMToken: ERC20Token;
  let ACDMPlatform: ACDMPlatform;

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    chairPerson = accounts[chairPersonId];

    XXXToken = await deploymentXXXToken();
    ACDMToken = await deploymentACDMToken();

    dao = await deployDAO();

    ACDMPlatform = await deploymentACDMPlatform(XXXToken, ACDMToken, dao);

    ACDMToken.grantRole(await ACDMToken.MINTER_ROLE(), ACDMPlatform.address);

    ACDMToken.connect(accounts[1]).approve(
      ACDMPlatform.address,
      ethers.constants.MaxUint256
    );

    await ACDMPlatform.connect(accounts[1]).signUpWithoutReferrer();
    await ACDMPlatform.changeState();

    await ACDMPlatform.connect(accounts[1]).buyTokensOnSale({
      value: ethers.utils.parseEther("0.5"),
    });

    const timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + ((await ACDMPlatform.roundTimeSize()).toNumber() + 1),
    ]);

    await ACDMPlatform.changeState();

    await ACDMPlatform.connect(accounts[1]).initSell(
      purchasedOnSale,
      await ACDMPlatform.ACDMTokenPrice()
    );
  });

  it("Should transfer tokens on initSell", async function () {
    const ACDMPlatformBalanceTokenBefore = await ACDMToken.balanceOf(
      accounts[1].address
    );

    await ACDMPlatform.connect(accounts[1]).stopSell();

    const ACDMPlatformBalanceTokenAfter = await ACDMToken.balanceOf(
      accounts[1].address
    );

    expect(
      (await ACDMPlatformBalanceTokenAfter).sub(ACDMPlatformBalanceTokenBefore)
    ).to.eq(purchasedOnSale);
  });

  it("Should init correct TradeSale", async function () {
    await ACDMPlatform.connect(accounts[1]).stopSell();

    const tradeSell = await ACDMPlatform.getTradeSale(accounts[1].address);

    expect(tradeSell.amount).to.eq(0);
    expect(tradeSell.price).to.eq(0);
  });

  describe("Emit", function () {
    it("Emit StopTradeSell(address[1], tokenAmount, price)", async () => {
      const tradeSell = await ACDMPlatform.getTradeSale(accounts[1].address);

      await expect(ACDMPlatform.connect(accounts[1]).stopSell())
        .to.emit(ACDMPlatform, "StopTradeSell")
        .withArgs(accounts[1].address, tradeSell.amount, tradeSell.price);
    });
  });

  describe("Reverted", function () {
    it("ACDMPlatform: not have sale", async () => {
      await ACDMPlatform.connect(accounts[1]).stopSell();

      await expect(
        ACDMPlatform.connect(accounts[1]).stopSell()
      ).to.be.revertedWith("ACDMPlatform: not have sale");
    });
  });
});
