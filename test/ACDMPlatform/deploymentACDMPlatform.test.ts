const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import {
  ACDMPlatform,
  DAO,
  ERC20Token,
  IERC20,
  Staking,
} from "../../src/types";
import { chairPersonId, deployDAO } from "../DAO/deploymentDAO";
import { deploymentXXXToken } from "../XXXToken/deployXXXToken";
import { deploymentACDMToken } from "../ACDMToken/deployACDMToken";
import {
  deploymentACDMPlatform,
  startPrice,
  uniswapRouterAddress,
} from "./deploymentACDMPlatform";

describe("Deployment ACDMPlatform", function () {
  let dao: DAO;
  let accounts: SignerWithAddress[];
  let chairPerson: SignerWithAddress;
  let XXXToken: ERC20Token;
  let lpToken: IERC20;
  let calldata: string;
  let recipient: string;
  let stakingContract: Staking;
  let ACDMToken: ERC20Token;
  let ACDMPlatform: ACDMPlatform;

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    chairPerson = accounts[chairPersonId];

    XXXToken = await deploymentXXXToken();
    ACDMToken = await deploymentACDMToken();
    dao = await deployDAO();

    ACDMPlatform = await deploymentACDMPlatform(XXXToken, ACDMToken, dao);
  });

  it("Should set the right XXXTokenAddress", async function () {
    expect(await ACDMPlatform.XXXTokenAddress()).to.equal(XXXToken.address);
  });

  it("Should set the right ACDMTokenAddress", async function () {
    expect(await ACDMPlatform.ACDMTokenAddress()).to.equal(ACDMToken.address);
  });

  it("Should set the right daoAddress", async function () {
    expect(await ACDMPlatform.daoAddress()).to.equal(dao.address);
  });

  it("Should set the right ownerAddress", async function () {
    expect(await ACDMPlatform.ownerAddress()).to.equal(accounts[0].address);
  });

  it("Should set the right ACDMTokenPrice", async function () {
    expect(await ACDMPlatform.ACDMTokenPrice()).to.deep.equal(startPrice);
  });
});
