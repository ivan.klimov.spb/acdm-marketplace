const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import {
  ACDMPlatform,
  DAO,
  ERC20Token,
  IERC20,
  Staking,
} from "../../src/types";
import { chairPersonId, deployDAO } from "../DAO/deploymentDAO";
import { deploymentXXXToken } from "../XXXToken/deployXXXToken";
import { deploymentACDMToken } from "../ACDMToken/deployACDMToken";
import {
  deploymentACDMPlatform,
  PlatformState,
  startPrice,
  uniswapRouterAddress,
} from "./deploymentACDMPlatform";
import { getTimestamp } from "../getTimestamp";
import { BigNumber } from "ethers";
import { addLiquidityETH } from "../Staking/addLiquidity";

const purchasedOnSale: BigNumber = BigNumber.from(50000000000);

describe("onlyDAO Fee ACDMPlatform", function () {
  let dao: DAO;
  let accounts: SignerWithAddress[];
  let chairPerson: SignerWithAddress;
  let XXXToken: ERC20Token;
  let lpToken: IERC20;
  let calldata: string;
  let recipient: string;
  let stakingContract: Staking;
  let ACDMToken: ERC20Token;
  let ACDMPlatform: ACDMPlatform;
  let valueForSale = ethers.utils.parseEther("1.0");

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    chairPerson = accounts[chairPersonId];

    XXXToken = await deploymentXXXToken();
    ACDMToken = await deploymentACDMToken();

    lpToken = await addLiquidityETH(XXXToken);
    await addLiquidityETH(XXXToken, 1);

    const factory = await ethers.getContractFactory("ACDMPlatform");
    ACDMPlatform = await factory.deploy(
      XXXToken.address,
      ACDMToken.address,
      accounts[1].address
    );
    await ACDMPlatform.deployed();

    ACDMToken.grantRole(await ACDMToken.MINTER_ROLE(), ACDMPlatform.address);

    ACDMToken.connect(accounts[1]).approve(
      ACDMPlatform.address,
      ethers.constants.MaxUint256
    );

    await ACDMPlatform.connect(accounts[1]).signUpWithoutReferrer();
    await ACDMPlatform.connect(accounts[2]).signUpWithoutReferrer();
    await ACDMPlatform.changeState();

    await ACDMPlatform.connect(accounts[1]).buyTokensOnSale({
      value: valueForSale,
    });

    const timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + ((await ACDMPlatform.roundTimeSize()).toNumber() + 1),
    ]);
  });

  it("Should send Fee To Owner ", async function () {
    await expect(() =>
      ACDMPlatform.connect(accounts[1]).sendFeeToOwner()
    ).to.changeEtherBalances([accounts[0]], [valueForSale.mul(8).div(100)]);
  });

  it("Should swap Fee And Burn XXXToken", async function () {
    await expect(() =>
      ACDMPlatform.connect(accounts[1]).swapFeeAndBurn()
    ).to.changeEtherBalances(
      [ACDMPlatform],
      [valueForSale.mul(8).div(100).mul(-1)]
    );
  });
});
