const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import {
  ACDMPlatform,
  DAO,
  ERC20Token,
  IERC20,
  Staking,
} from "../../src/types";
import { chairPersonId, deployDAO } from "../DAO/deploymentDAO";
import { deploymentXXXToken } from "../XXXToken/deployXXXToken";
import { deploymentACDMToken } from "../ACDMToken/deployACDMToken";
import {
  deploymentACDMPlatform,
  PlatformState,
  startPrice,
  uniswapRouterAddress,
} from "./deploymentACDMPlatform";
import { getTimestamp } from "../getTimestamp";
import { BigNumber } from "ethers";

const purchasedOnSale: BigNumber = BigNumber.from(50000000000);

describe("onlyDAOSet ACDMPlatform", function () {
  let dao: DAO;
  let accounts: SignerWithAddress[];
  let chairPerson: SignerWithAddress;
  let XXXToken: ERC20Token;
  let lpToken: IERC20;
  let calldata: string;
  let recipient: string;
  let stakingContract: Staking;
  let ACDMToken: ERC20Token;
  let ACDMPlatform: ACDMPlatform;

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    chairPerson = accounts[chairPersonId];

    XXXToken = await deploymentXXXToken();
    ACDMToken = await deploymentACDMToken();

    const factory = await ethers.getContractFactory("ACDMPlatform");
    ACDMPlatform = await factory.deploy(
      XXXToken.address,
      ACDMToken.address,
      accounts[1].address
    );
    await ACDMPlatform.deployed();

    ACDMToken.grantRole(await ACDMToken.MINTER_ROLE(), ACDMPlatform.address);
  });

  it("Should set Percent Fee For First Level Sale", async function () {
    await ACDMPlatform.connect(accounts[1]).setPercentFeeForFirstLevelSale(100); //1%

    expect(await ACDMPlatform.percentageFirstReferrerSale()).to.eq(100);
  });

  it("Should set Percent Fee For Second Level Sale", async function () {
    await ACDMPlatform.connect(accounts[1]).setPercentFeeForSecondLevelSale(
      100
    ); //1%

    expect(await ACDMPlatform.percentageSecondReferrerSale()).to.eq(100);
  });

  it("Should set Percent Fee For Trading", async function () {
    await ACDMPlatform.connect(accounts[1]).setPercentFeeForTrading(100); //1%

    expect(await ACDMPlatform.percentageReferrerTrading()).to.eq(100);
  });

  describe("Reverted", function () {
    it("ACDMPlatform:too high percentage", async () => {
      await expect(
        ACDMPlatform.connect(accounts[1]).setPercentFeeForFirstLevelSale(20000)
      ).to.be.revertedWith("ACDMPlatform:too high percentage");
    });

    it("ACDMPlatform:too high percentage", async () => {
      await expect(
        ACDMPlatform.connect(accounts[1]).setPercentFeeForSecondLevelSale(20000)
      ).to.be.revertedWith("ACDMPlatform:too high percentage");
    });

    it("ACDMPlatform:too high percentage", async () => {
      await expect(
        ACDMPlatform.connect(accounts[1]).setPercentFeeForTrading(20000)
      ).to.be.revertedWith("ACDMPlatform:too high percentage");
    });

    it("ACDMPlatform: need dao role", async () => {
      await expect(
        ACDMPlatform.connect(accounts[2]).setPercentFeeForTrading(20000)
      ).to.be.revertedWith("ACDMPlatform: need dao role");
    });
  });
});
