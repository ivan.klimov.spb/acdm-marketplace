const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import {
  ACDMPlatform,
  DAO,
  ERC20Token,
  IERC20,
  Staking,
} from "../../src/types";
import { chairPersonId, deployDAO } from "../DAO/deploymentDAO";
import { deploymentXXXToken } from "../XXXToken/deployXXXToken";
import { deploymentACDMToken } from "../ACDMToken/deployACDMToken";
import {
  deploymentACDMPlatform,
  PlatformState,
  startPrice,
  uniswapRouterAddress,
} from "./deploymentACDMPlatform";
import { getTimestamp } from "../getTimestamp";
import { BigNumber } from "ethers";

describe("Change state ACDMPlatform", function () {
  let dao: DAO;
  let accounts: SignerWithAddress[];
  let chairPerson: SignerWithAddress;
  let XXXToken: ERC20Token;
  let lpToken: IERC20;
  let calldata: string;
  let recipient: string;
  let stakingContract: Staking;
  let ACDMToken: ERC20Token;
  let ACDMPlatform: ACDMPlatform;

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    chairPerson = accounts[chairPersonId];

    XXXToken = await deploymentXXXToken();
    ACDMToken = await deploymentACDMToken();

    /*await XXXToken.mint(accounts[1].address, MINT_XXX_TOKEN.mul(2));
    lpToken = await addLiquidityETH(XXXToken, 1);

    stakingContract = await initStakingForTests(XXXToken, lpToken, accounts);*/

    dao = await deployDAO();

    ACDMPlatform = await deploymentACDMPlatform(XXXToken, ACDMToken, dao);

    ACDMToken.grantRole(await ACDMToken.MINTER_ROLE(), ACDMPlatform.address);

    ACDMToken.connect(accounts[1]).approve(
      ACDMPlatform.address,
      ethers.constants.MaxUint256
    );
  });

  it("Should state NOT_STARTED by default", async function () {
    expect(await ACDMPlatform.state()).to.equal(PlatformState.NOT_STARTED);
  });

  it("Should  numberPlatformCircle = 0 by default", async function () {
    expect(await ACDMPlatform.state()).to.equal(PlatformState.NOT_STARTED);
  });

  it("Should state SALE after first call changeState", async function () {
    await ACDMPlatform.changeState();

    expect(await ACDMPlatform.state()).to.equal(PlatformState.SALE);
  });

  it("Should numberPlatformCircle = 1 after first call changeState", async function () {
    await ACDMPlatform.changeState();

    expect(await ACDMPlatform.numberPlatformCircle()).to.equal(1);
  });

  it("Should ACDMToken balance 100000 after first call changeState", async function () {
    await ACDMPlatform.changeState();

    let ACDMTokensOnStart = await ACDMToken.balanceOf(ACDMPlatform.address);

    expect(ACDMTokensOnStart).to.equal(BigNumber.from(100000000000));
  });

  it("Should state TRADE after time and second call", async function () {
    await ACDMPlatform.changeState();

    const timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + ((await ACDMPlatform.roundTimeSize()).toNumber() + 1),
    ]);

    await ACDMPlatform.changeState();

    expect(await ACDMPlatform.state()).to.equal(PlatformState.TRADE);
  });

  it("Should burn ACDMTokens after sale round", async function () {
    await ACDMPlatform.changeState();

    const timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + ((await ACDMPlatform.roundTimeSize()).toNumber() + 1),
    ]);

    await ACDMPlatform.changeState();

    let ACDMTokensAfter = await ACDMToken.balanceOf(ACDMPlatform.address);

    expect(ACDMTokensAfter).to.equal(0);
  });

  it("Can't burn ACDMTokens after sale round because all was bought", async function () {
    await ACDMPlatform.connect(accounts[1]).signUpWithoutReferrer();
    await ACDMPlatform.changeState();

    await ACDMPlatform.connect(accounts[1]).buyTokensOnSale({
      value: ethers.utils.parseEther("2"),
    });

    const timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + ((await ACDMPlatform.roundTimeSize()).toNumber() + 1),
    ]);

    await ACDMPlatform.changeState();

    let ACDMTokensAfter = await ACDMToken.balanceOf(ACDMPlatform.address);

    expect(ACDMTokensAfter).to.equal(0);
  });

  //trade

  it("After trade correct new price for sale", async function () {
    await ACDMPlatform.connect(accounts[1]).signUpWithoutReferrer();
    await ACDMPlatform.connect(accounts[2]).signUpWithoutReferrer();
    await ACDMPlatform.changeState();
    //sale

    let tokenAmount = await ACDMToken.balanceOf(ACDMPlatform.address);
    let tokenPrice = await ACDMPlatform.ACDMTokenPrice();

    await ACDMPlatform.connect(accounts[1]).buyTokensOnSale({
      value: ethers.utils.parseEther("2"),
    });

    let timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + ((await ACDMPlatform.roundTimeSize()).toNumber() + 1),
    ]);

    await ACDMPlatform.changeState();
    //trade

    await ACDMPlatform.connect(accounts[1]).initSell(tokenAmount, tokenPrice);
    await ACDMPlatform.connect(accounts[2]).buyTradeSell(accounts[1].address, {
      value: ethers.utils.parseEther("1"),
    });

    timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + ((await ACDMPlatform.roundTimeSize()).toNumber() + 1),
    ]);

    await ACDMPlatform.changeState();
    //sale
    let newTokenPrice = tokenPrice
      .mul(103)
      .div(100)
      .add(ethers.utils.parseEther("0.000004"));

    expect(await ACDMPlatform.ACDMTokenPrice()).to.equal(newTokenPrice);
  });

  it("After trade correct saleTokenAmount minted for sale", async function () {
    await ACDMPlatform.connect(accounts[1]).signUpWithoutReferrer();
    await ACDMPlatform.connect(accounts[2]).signUpWithoutReferrer();
    await ACDMPlatform.changeState();
    //sale

    let tokenAmount = await ACDMToken.balanceOf(ACDMPlatform.address);
    let tokenPrice = await ACDMPlatform.ACDMTokenPrice();

    await ACDMPlatform.connect(accounts[1]).buyTokensOnSale({
      value: ethers.utils.parseEther("2"),
    });

    let timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + ((await ACDMPlatform.roundTimeSize()).toNumber() + 1),
    ]);

    await ACDMPlatform.changeState();
    //trade

    await ACDMPlatform.connect(accounts[1]).initSell(tokenAmount, tokenPrice);
    await ACDMPlatform.connect(accounts[2]).buyTradeSell(accounts[1].address, {
      value: ethers.utils.parseEther("1"),
    });

    timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + ((await ACDMPlatform.roundTimeSize()).toNumber() + 1),
    ]);

    let valume = await ACDMPlatform.lastTradingVolume();

    await ACDMPlatform.changeState();
    //sale
    let newTokenPrice = await ACDMPlatform.ACDMTokenPrice();

    expect(await ACDMPlatform.saleTokenAmount()).to.equal(
      valume.div(newTokenPrice)
    );

    expect(await ACDMToken.balanceOf(ACDMPlatform.address)).to.equal(
      await ACDMPlatform.saleTokenAmount()
    );
  });

  it("After trade little traded and again trade round", async function () {
    await ACDMPlatform.connect(accounts[1]).signUpWithoutReferrer();
    await ACDMPlatform.connect(accounts[2]).signUpWithoutReferrer();
    await ACDMPlatform.changeState();
    //sale

    let tokenAmount = await ACDMToken.balanceOf(ACDMPlatform.address);
    let tokenPrice = await ACDMPlatform.ACDMTokenPrice();

    await ACDMPlatform.connect(accounts[1]).buyTokensOnSale({
      value: ethers.utils.parseEther("2"),
    });

    let timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + ((await ACDMPlatform.roundTimeSize()).toNumber() + 1),
    ]);

    await ACDMPlatform.changeState();
    //trade

    await ACDMPlatform.connect(accounts[1]).initSell(tokenAmount, tokenPrice);

    timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + ((await ACDMPlatform.roundTimeSize()).toNumber() + 1),
    ]);

    await ACDMPlatform.changeState();
    //trade again

    expect(await ACDMPlatform.state()).to.equal(PlatformState.TRADE);
  });

  describe("Reverted", function () {
    it("ACDMPlatform: sale is not over", async () => {
      await ACDMPlatform.connect(accounts[1]).signUpWithoutReferrer();
      await ACDMPlatform.changeState();

      await expect(ACDMPlatform.changeState()).to.be.revertedWith(
        "ACDMPlatform: sale is not over"
      );
    });

    it("ACDMPlatform:trading is not over", async () => {
      await ACDMPlatform.connect(accounts[1]).signUpWithoutReferrer();
      await ACDMPlatform.changeState();

      let timeStamp = await getTimestamp();
      await ethers.provider.send("evm_mine", [
        timeStamp + ((await ACDMPlatform.roundTimeSize()).toNumber() + 1),
      ]);

      await ACDMPlatform.changeState();

      await expect(ACDMPlatform.changeState()).to.be.revertedWith(
        "ACDMPlatform:trading is not over"
      );
    });
  });
});
