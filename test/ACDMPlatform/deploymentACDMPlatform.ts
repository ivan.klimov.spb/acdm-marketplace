import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { ethers } from "hardhat";
import { ACDMPlatform, DAO, ERC20Token } from "../../src/types";
import { BigNumber } from "ethers";

export const uniswapRouterAddress =
  "0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D";
export const startPrice = ethers.utils.parseEther("1").div(100000000000);
export enum PlatformState {
  NOT_STARTED,
  SALE,
  TRADE,
}
export async function deploymentACDMPlatform(
  XXXToken: ERC20Token,
  ACDMToken: ERC20Token,
  dao: DAO
) {
  const factory = await ethers.getContractFactory("ACDMPlatform");
  let ACDMPlatform = await factory.deploy(
    XXXToken.address,
    ACDMToken.address,
    dao.address
  );
  await ACDMPlatform.deployed();

  return ACDMPlatform;
}
