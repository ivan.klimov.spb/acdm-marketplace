const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import {
  ERC20Token,
  IERC20,
  IUniswapV2Router01,
  IUniswapV2Factory,
  Staking,
} from "../../src/types";
import { BigNumber } from "ethers";

import { deploymentXXXToken } from "../XXXToken/deployXXXToken";
import { addLiquidityETH } from "./addLiquidity";
import { deployStaking } from "./deploymentStaking";

describe("Deployment Staking", function () {
  let XXXToken: ERC20Token;
  let lpToken: IERC20;
  let accounts: SignerWithAddress[];
  let stakingContract: Staking;

  beforeEach(async function () {
    accounts = await ethers.getSigners();

    XXXToken = await deploymentXXXToken();

    lpToken = await addLiquidityETH(XXXToken);

    const factoryStaking = await ethers.getContractFactory("Staking");
    stakingContract = await factoryStaking.deploy(
      lpToken.address,
      XXXToken.address
    );
    await stakingContract.deployed();

    XXXToken.grantRole(await XXXToken.MINTER_ROLE(), stakingContract.address);
  });
  it("Should set the right lpToken", async function () {
    expect(await stakingContract.lpToken()).to.equal(lpToken.address);
  });

  it("Should set the right XXXToken", async function () {
    expect(await stakingContract.rewardableToken()).to.equal(XXXToken.address);
  });

  it("Should set the right lpToken after test script deploy", async function () {
    lpToken = await addLiquidityETH(XXXToken);
    let StakingFromScript: Staking = await deployStaking(lpToken, XXXToken);
    expect(await StakingFromScript.lpToken()).to.equal(lpToken.address);
  });

  it("Should set the right XXXToken after test script deploy", async function () {
    lpToken = await addLiquidityETH(XXXToken);
    let StakingFromScript: Staking = await deployStaking(lpToken, XXXToken);
    expect(await StakingFromScript.rewardableToken()).to.equal(
      XXXToken.address
    );
  });
});
