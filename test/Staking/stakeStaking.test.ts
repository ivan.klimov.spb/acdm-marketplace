const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { ERC20Token, IERC20, Staking } from "../../src/types";
import { BigNumber } from "ethers";

import { deploymentXXXToken } from "../XXXToken/deployXXXToken";
import { addLiquidityETH } from "./addLiquidity";
import { defaultStakeAmount, deployStaking } from "./deploymentStaking";
import { getTimestamp } from "../getTimestamp";

describe("Stake Staking", function () {
  let XXXToken: ERC20Token;
  let lpToken: IERC20;
  let accounts: SignerWithAddress[];
  let stakingContract: Staking;

  beforeEach(async function () {
    accounts = await ethers.getSigners();

    XXXToken = await deploymentXXXToken();

    lpToken = await addLiquidityETH(XXXToken);
    await addLiquidityETH(XXXToken, 1);

    stakingContract = await deployStaking(lpToken, XXXToken);

    await XXXToken.grantRole(
      await XXXToken.MINTER_ROLE(),
      stakingContract.address
    );

    lpToken
      .connect(accounts[0])
      .approve(stakingContract.address, ethers.constants.MaxUint256);

    lpToken
      .connect(accounts[1])
      .approve(stakingContract.address, ethers.constants.MaxUint256);
  });

  it("Should set the right stake amount", async function () {
    let stakeLPTokens = await lpToken.balanceOf(accounts[1].address);

    await stakingContract.connect(accounts[1]).stake(stakeLPTokens);

    expect(
      await stakingContract.getLockedTokenAmount(accounts[1].address)
    ).to.equal(stakeLPTokens);
  });

  it("Should set the right stake reward", async function () {
    let stakeLPTokens = await lpToken.balanceOf(accounts[1].address);

    await stakingContract.connect(accounts[1]).stake(stakeLPTokens);

    expect(
      await stakingContract.getCurrentSaveReward(accounts[1].address)
    ).to.equal(0);
  });

  it("Should set the right stake startTime", async function () {
    let stakeLPTokens = await lpToken.balanceOf(accounts[1].address);

    await stakingContract.connect(accounts[1]).stake(stakeLPTokens);

    expect(
      await stakingContract.getTimeStartStake(accounts[1].address)
    ).to.equal(await getTimestamp());
  });

  it("Should set the right update stake amount", async function () {
    await stakingContract.connect(accounts[1]).stake(defaultStakeAmount);

    const timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + ((await stakingContract.rewardCircleTimer()) + 1),
    ]);

    await stakingContract.connect(accounts[1]).stake(defaultStakeAmount);

    expect(
      await stakingContract.getLockedTokenAmount(accounts[1].address)
    ).to.equal(defaultStakeAmount + defaultStakeAmount);
  });

  it("Should set the right update stake reward", async function () {
    await stakingContract.connect(accounts[1]).stake(defaultStakeAmount);

    const timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + ((await stakingContract.rewardCircleTimer()) + 1),
    ]);

    await stakingContract.connect(accounts[1]).stake(defaultStakeAmount);

    expect(
      await stakingContract.getCurrentSaveReward(accounts[1].address)
    ).to.equal(300);
  });

  describe("Emit", function () {
    it("Emit Staked(address[1], 10000, now)", async () => {
      await expect(
        stakingContract.connect(accounts[1]).stake(defaultStakeAmount)
      )
        .to.emit(stakingContract, "Staked")
        .withArgs(
          accounts[1].address,
          defaultStakeAmount,
          (await getTimestamp()) + 1
        );
    });
  });
});
