const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { ERC20Token, IERC20, Staking } from "../../src/types";
import { BigNumber } from "ethers";

import { deploymentXXXToken } from "../XXXToken/deployXXXToken";
import { addLiquidityETH } from "./addLiquidity";
import { deployStaking } from "./deploymentStaking";

describe("SetLockTime Staking", function () {
  let XXXToken: ERC20Token;
  let lpToken: IERC20;
  let accounts: SignerWithAddress[];
  let stakingContract: Staking;

  beforeEach(async function () {
    accounts = await ethers.getSigners();

    XXXToken = await deploymentXXXToken();

    lpToken = await addLiquidityETH(XXXToken);
    await addLiquidityETH(XXXToken, 1);

    stakingContract = await deployStaking(lpToken, XXXToken);

    stakingContract.setDAO(accounts[1].address);
  });

  it("Should be set right locktime", async function () {
    await stakingContract.connect(accounts[1]).setLockTime(100);

    expect(await stakingContract.lockTime()).to.equal(100);
  });

  describe("Reverted", function () {
    it("Staking: need dao role", async () => {
      await expect(stakingContract.setLockTime(100)).to.be.revertedWith(
        "Staking: need dao role"
      );
    });
  });

  describe("Emit", function () {
    it("Emit LockTimeUpdated(100)", async () => {
      await expect(stakingContract.connect(accounts[1]).setLockTime(100))
        .to.emit(stakingContract, "LockTimeUpdated")
        .withArgs(100);
    });
  });
});
