const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import {
  ERC20Token,
  IERC20,
  IUniswapV2Router01,
  IUniswapV2Factory,
} from "../../src/types";
import { BigNumber } from "ethers";

import {
  NAME_XXX_TOKEN,
  SYMBOL_XXX_TOKEN,
  DECIMALS_XXX_TOKEN,
  MINT_XXX_TOKEN,
} from "../XXXToken/deployXXXToken";
import { addLiquidityETH } from "./addLiquidity";
import { getTimestamp } from "../getTimestamp";

describe("Add Liquidity", function () {
  let XXXToken: ERC20Token;
  let accounts: SignerWithAddress[];
  let uniswapRoter: IUniswapV2Router01;
  let uniswapFactory: IUniswapV2Factory;

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    const factoryXXXToken = await ethers.getContractFactory("ERC20Token");
    XXXToken = await factoryXXXToken.deploy(
      NAME_XXX_TOKEN,
      SYMBOL_XXX_TOKEN,
      DECIMALS_XXX_TOKEN
    );
    await XXXToken.deployed();

    await XXXToken.mint(accounts[0].address, MINT_XXX_TOKEN);

    uniswapFactory = <IUniswapV2Factory>(
      await ethers.getContractAt(
        "IUniswapV2Factory",
        process.env.FACTORY_ADDRESS as string
      )
    );

    uniswapRoter = <IUniswapV2Router01>(
      await ethers.getContractAt(
        "IUniswapV2Router01",
        process.env.ROUTER_ADDRESS as string
      )
    );
  });

  it("liquidity pare not added", async () => {
    const wethTokenAddr = await uniswapRoter.WETH();
    let address = await uniswapFactory.getPair(XXXToken.address, wethTokenAddr);
    assert.deepEqual(address, ethers.constants.AddressZero);
  });

  it("liquidity pare added", async () => {
    const wethTokenAddr = await uniswapRoter.WETH();
    await uniswapFactory.createPair(XXXToken.address, wethTokenAddr);
    let address = await uniswapFactory.getPair(XXXToken.address, wethTokenAddr);
    assert.notDeepEqual(address, ethers.constants.AddressZero);
  });

  it("liquidity add LP tokens", async () => {
    await XXXToken.approve(uniswapRoter.address, ethers.constants.MaxUint256);

    const timestemp: number = await getTimestamp();

    let result = await uniswapRoter.addLiquidityETH(
      XXXToken.address,
      MINT_XXX_TOKEN,
      MINT_XXX_TOKEN,
      ethers.utils.parseEther("1"),
      accounts[0].address,
      timestemp + 30,
      {
        value: ethers.utils.parseEther("1"), //eth value
      }
    );

    const wethTokenAddr = await uniswapRoter.WETH();
    let addressLP = await uniswapFactory.getPair(
      XXXToken.address,
      wethTokenAddr
    );

    let tokenLP = await ethers.getContractAt("IERC20", addressLP);

    const balanceLP = await tokenLP.balanceOf(accounts[0].address);

    assert.notDeepEqual(addressLP, ethers.constants.AddressZero);
  });

  it("liquidity add LP tokens from function", async () => {
    let lpToken = await addLiquidityETH(XXXToken);

    const balanceLP = await lpToken.balanceOf(accounts[0].address);

    assert.notDeepEqual(lpToken.address, ethers.constants.AddressZero);
  });

  it("liquidity add LP tokens for two accounts from function", async () => {
    let lpToken = await addLiquidityETH(XXXToken);
    await addLiquidityETH(XXXToken, 1);

    const balanceLP = await lpToken.balanceOf(accounts[1].address);

    expect(balanceLP.gt(0)).to.be.true;
  });
});
