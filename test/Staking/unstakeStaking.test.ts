const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import {
  DAO,
  ERC20Token,
  ERC20Token__factory,
  IERC20,
  Staking,
} from "../../src/types";
import { BigNumber } from "ethers";

import { deploymentXXXToken, MINT_XXX_TOKEN } from "../XXXToken/deployXXXToken";
import { addLiquidityETH } from "./addLiquidity";
import { defaultStakeAmount, deployStaking } from "./deploymentStaking";
import { getTimestamp } from "../getTimestamp";
import { chairPersonId, deployDAO } from "../DAO/deploymentDAO";

describe("Unstake Staking", function () {
  let XXXToken: ERC20Token;
  let lpToken: IERC20;
  let accounts: SignerWithAddress[];
  let stakingContract: Staking;
  let dao: DAO;
  let balanceOfLPTokensBeforStakeAcc1: BigNumber;

  beforeEach(async function () {
    accounts = await ethers.getSigners();

    XXXToken = await deploymentXXXToken();

    lpToken = await addLiquidityETH(XXXToken);
    await addLiquidityETH(XXXToken, 1);

    stakingContract = await deployStaking(lpToken, XXXToken);
    dao = await deployDAO();

    stakingContract.setDAO(dao.address);

    await XXXToken.grantRole(
      await XXXToken.MINTER_ROLE(),
      stakingContract.address
    );

    lpToken
      .connect(accounts[0])
      .approve(stakingContract.address, ethers.constants.MaxUint256);

    lpToken
      .connect(accounts[1])
      .approve(stakingContract.address, ethers.constants.MaxUint256);

    balanceOfLPTokensBeforStakeAcc1 = await lpToken.balanceOf(
      accounts[1].address
    );
    await stakingContract.connect(accounts[1]).stake(defaultStakeAmount);

    const timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + ((await stakingContract.lockTime()).toNumber() + 1),
    ]);
  });

  it("Should unstake with the right reward", async function () {
    let balanceBeforeClaim = (
      await XXXToken.balanceOf(accounts[1].address)
    ).toNumber();

    await stakingContract.connect(accounts[1]).unstake();

    let balanceAfterClaim = (
      await XXXToken.balanceOf(accounts[1].address)
    ).toNumber();

    expect(balanceAfterClaim - balanceBeforeClaim).to.equal(600);
  });

  it("Should unstake and back lpTokens to user", async function () {
    let balanceBeforeUnstake = await lpToken.balanceOf(accounts[1].address);

    await stakingContract.connect(accounts[1]).unstake();

    let balanceAfterUnstake = await lpToken.balanceOf(accounts[1].address);

    expect(balanceAfterUnstake.sub(balanceBeforeUnstake).toNumber()).to.equal(
      defaultStakeAmount
    );
  });

  it("Should same balance before stake", async function () {
    await stakingContract.connect(accounts[1]).unstake();

    let balanceAfterUnstake = await lpToken.balanceOf(accounts[1].address);

    expect(balanceAfterUnstake).to.deep.equal(balanceOfLPTokensBeforStakeAcc1);
  });

  describe("Emit", function () {
    it("Emit Unstaked(address[1], 600)", async () => {
      await expect(stakingContract.connect(accounts[1]).unstake())
        .to.emit(stakingContract, "Unstaked")
        .withArgs(
          accounts[1].address,
          defaultStakeAmount,
          (await getTimestamp()) + 1
        );
    });
  });

  describe("Reverted", function () {
    it("Staking:lock time not passed", async () => {
      await stakingContract.connect(accounts[1]).stake(defaultStakeAmount);

      await expect(
        stakingContract.connect(accounts[1]).unstake()
      ).to.be.revertedWith("Staking:lock time not passed");
    });

    it("Staking:votes not finished", async () => {
      await stakingContract.connect(accounts[1]).stake(defaultStakeAmount);
      let chairPerson = accounts[chairPersonId];

      let calldata = ERC20Token__factory.createInterface().encodeFunctionData(
        "mint",
        [accounts[1].address, MINT_XXX_TOKEN]
      );

      let recipient = XXXToken.address;

      await dao.connect(chairPerson).addProposal(calldata, recipient);
      await dao.connect(chairPerson).setStakingAddress(stakingContract.address);

      let AGREE = true;

      await dao.connect(accounts[1]).vote(1, AGREE);

      const timeStamp = await getTimestamp();
      await ethers.provider.send("evm_mine", [
        timeStamp + ((await stakingContract.lockTime()).toNumber() + 1),
      ]);

      await expect(
        stakingContract.connect(accounts[1]).unstake()
      ).to.be.revertedWith("Staking: votes not finished");
    });
  });
});
