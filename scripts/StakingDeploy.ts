import dotenv from "dotenv";
dotenv.config({ path: __dirname + "/.env" });

import { ethers } from "hardhat";
import { Staking } from "../src/types";

const { LP_TOKEN_ADDRESS, XXX_TOKEN_ADDRESS } = process.env;

async function main() {
  const factory = await ethers.getContractFactory("Staking");
  console.log("Deploying Staking...");

  const contract: Staking = await factory.deploy(
    LP_TOKEN_ADDRESS as string,
    XXX_TOKEN_ADDRESS as string
  );

  await contract.deployed();

  console.log("Staking deployed to: ", contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
