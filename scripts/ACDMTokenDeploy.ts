import dotenv from "dotenv";
dotenv.config({ path: __dirname + "/.env" });

import { ethers } from "hardhat";
import { ERC20Token } from "../src/types";

async function main() {
  const factory = await ethers.getContractFactory("ERC20Token");
  console.log("Deploying ACDMToken...");

  const contract: ERC20Token = await factory.deploy("ACADEM Coin", "ACDM", 6);

  await contract.deployed();

  console.log("ACDMToken deployed to:", contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
