import dotenv from "dotenv";
dotenv.config({ path: __dirname + "/.env" });

import { ethers } from "hardhat";
import { ACDMPlatform } from "../src/types";

const { XXX_TOKEN_ADDRESS, ACDM_TOKEN_ADDRESS, DAO_ADDRESS } = process.env;

async function main() {
  const factory = await ethers.getContractFactory("ACDMPlatform");
  console.log("Deploying ACDMPlatform...");

  const contract: ACDMPlatform = await factory.deploy(
    XXX_TOKEN_ADDRESS as string,
    ACDM_TOKEN_ADDRESS as string,
    DAO_ADDRESS as string
  );

  await contract.deployed();

  console.log("ACDMPlatform deployed to: ", contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
