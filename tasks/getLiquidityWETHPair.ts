import { TASK_GET_LIQUIDITY_WETH_PAIR } from "./task-names";
import { task } from "hardhat/config";

import dotenv from "dotenv";
dotenv.config({ path: __dirname + "/.env" });

const { PUBLIC_KEY } = process.env;

task(TASK_GET_LIQUIDITY_WETH_PAIR, "get liquidity WETH pair from uniswap")
  .addParam("token", "token address ")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(PUBLIC_KEY as string);

    let uniswapV2Router01 = await hre.ethers.getContractAt(
      "IUniswapV2Router01",
      process.env.ROUTER_ADDRESS as string
    );

    let uniswapV2Factory = await hre.ethers.getContractAt(
      "IUniswapV2Factory",
      process.env.FACTORY_ADDRESS as string
    );

    const wethTokenAddr = await uniswapV2Router01.connect(account).WETH();
    let addressLP = await uniswapV2Factory
      .connect(account)
      .getPair(args.token, wethTokenAddr);

    console.log("Address LP token: " + addressLP);
  });
