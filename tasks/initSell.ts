import { task } from "hardhat/config";
import { TASK_INIT_SELL } from "./task-names";

task(TASK_INIT_SELL, "init sell in trade round ACDMPlatform")
  .addParam("contract", "contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("amount", "Token amount")
  .addParam("price", "Token price")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let ACDMPlatform = await hre.ethers.getContractAt(
      "ACDMPlatform",
      args.contract
    );

    await ACDMPlatform.connect(account).initSell(args.amount, args.price);

    console.log("task init sell in trade round finished");
  });
