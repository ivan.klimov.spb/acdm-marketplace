import { task } from "hardhat/config";
import { TASK_SIGN_UP_WITHOUT_REFERRER } from "./task-names";

task(TASK_SIGN_UP_WITHOUT_REFERRER, "sign up without referrer ACDMPlatform")
  .addParam("contract", "contract address")
  .addParam("signer", "Account signing the transaction")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let ACDMPlatform = await hre.ethers.getContractAt(
      "ACDMPlatform",
      args.contract
    );

    await ACDMPlatform.connect(account).signUpWithoutReferrer();

    console.log("task sign up without referrer finished");
  });
