import { task } from "hardhat/config";
import { TASK_STAKE } from "./task-names";

task(TASK_STAKE, "Stake tokens")
  .addParam("contract", "contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("amount", "Amount of tokens")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let staking = await hre.ethers.getContractAt("Staking", args.contract);

    await staking.connect(account).stake(args.amount);

    console.log("task stake finished");
  });
