import { task } from "hardhat/config";
import { TASK_BUY_TOKENS_ON_SALE } from "./task-names";

task(TASK_BUY_TOKENS_ON_SALE, "buy token on sale ACDMPlatform")
  .addParam("contract", "contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("eth", "amount of eth")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let ACDMPlatform = await hre.ethers.getContractAt(
      "ACDMPlatform",
      args.contract
    );

    await ACDMPlatform.connect(account).buyTokensOnSale({
      value: args.eth, //eth value
      gasLimit: 300000,
    });

    console.log("task buy token on sale finished");
  });
