import "./task-names";
import "./mint";
import "./approve";
import "./addLiquidityETH";
import "./getLiquidityWETHPair";
import "./addMintPermission";

//Staking
import "./stake";
import "./claim";
import "./unstake";

//DAO
import "./addProposal";
import "./vote";
import "./finishVoting";

//ACDMPlatform
import "./buyTokensOnSale";
import "./buyTradeSell";
import "./initSell";
import "./stopSell";
import "./changeState";

import "./signUpWithoutReferrer";
import "./signUp";
import "./setReferrer";
