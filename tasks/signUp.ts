import { task } from "hardhat/config";
import { TASK_SIGN_UP } from "./task-names";

task(TASK_SIGN_UP, "sign Up ACDMPlatform")
  .addParam("contract", "contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("referrer", "referrer address")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let ACDMPlatform = await hre.ethers.getContractAt(
      "ACDMPlatform",
      args.contract
    );

    await ACDMPlatform.connect(account).signUp(args.referrer);

    console.log("task sign Up finished");
  });
