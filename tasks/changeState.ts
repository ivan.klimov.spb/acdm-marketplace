import { task } from "hardhat/config";
import { TASK_CHANGE_PLATFORM_STATE } from "./task-names";

task(TASK_CHANGE_PLATFORM_STATE, "change state ACDMPlatform")
  .addParam("contract", "contract address")
  .addParam("signer", "Account signing the transaction")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let ACDMPlatform = await hre.ethers.getContractAt(
      "ACDMPlatform",
      args.contract
    );

    await ACDMPlatform.connect(account).changeState({
      gasLimit: 300000,
    });

    console.log("task changeState finished");
  });
