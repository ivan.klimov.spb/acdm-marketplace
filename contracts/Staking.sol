// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;

//import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "./IERC20Mintable.sol";
import "./IStaking.sol";
import "./IDAO.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract Staking is IStaking, Ownable {// is AccessControl {

//requer mint for rewardableToken

    struct StakeHolder {
        //uint64 id;
        uint64 reward;
        uint64 timeStartStake;
        uint128 amount;
    }

    uint16 public rewardPercentage;
    uint64 public lockTime;
    uint32 public rewardCircleTimer;
    
    address public lpToken;
    address public rewardableToken;
    address public dao;

    mapping (address => StakeHolder) _stakeHolders;

    constructor(address lpToken_, address rewardableToken_) {
        lpToken = lpToken_;
        rewardableToken = rewardableToken_;

        rewardCircleTimer = 1 weeks; // 1 week
        rewardPercentage = 300; //3.00 %
        lockTime = 2 weeks; //2 weeks
    }

    modifier onlyDAO() {
        require(dao == msg.sender, "Staking: need dao role");
        _;
    }

    function getLockedTokenAmount(address accountAddress) external view virtual override returns(uint128) {
        return _stakeHolders[accountAddress].amount;
    }

    function getCurrentSaveReward(address accountAddress) external view virtual override returns(uint64) {
        return _stakeHolders[accountAddress].reward;
    }

    function getTimeStartStake(address accountAddress) external view virtual override returns(uint64) {
        return _stakeHolders[accountAddress].timeStartStake;
    }

    function setDAO(address newDAOAddress) external virtual override onlyOwner {
        dao = newDAOAddress;
    }

    function stake(uint128 amount) external virtual override {
        address sender = msg.sender;
        IERC20(lpToken).transferFrom(sender, address(this), amount);

        StakeHolder storage stakeHolder = _stakeHolders[sender];
        uint64 timeStamp = uint64(block.timestamp);

        //calculate current reward
        uint128 amountInStake = stakeHolder.amount;
        uint64 countCycles = (timeStamp - stakeHolder.timeStartStake) / rewardCircleTimer;
        uint256 reward = amountInStake * countCycles * rewardPercentage / 10000; // / 100.00%

        //update stake data
        stakeHolder.timeStartStake = timeStamp;
        stakeHolder.reward += uint64(reward);
        stakeHolder.amount += amount;

        emit Staked(sender, amount, timeStamp);
    }

    function claim() public virtual override {
        address sender = msg.sender;
        StakeHolder storage stakeHolder = _stakeHolders[sender];

        require(stakeHolder.amount > 0, "Staking: User has not staked yet");
        
        uint64 timeStamp = uint64(block.timestamp);
      
        //calculate current reward
        uint256 amountInStake = stakeHolder.amount;
        uint256 countCycles = (timeStamp - stakeHolder.timeStartStake) / rewardCircleTimer;
        uint256 reward = amountInStake * countCycles * rewardPercentage / 10000; // / 100.00%
        reward += stakeHolder.reward;
        
        stakeHolder.timeStartStake = timeStamp;
        stakeHolder.reward = 0;

        IERC20Mintable(rewardableToken).mint(sender, reward);

        emit Claimed(sender, reward);
    }

    function unstake() external virtual override {
        address sender = msg.sender;
        StakeHolder storage stakeHolder = _stakeHolders[sender];

        uint64 timeStamp = uint64(block.timestamp);
        require(timeStamp > (stakeHolder.timeStartStake + lockTime), "Staking:lock time not passed");
        
        require(IDAO(dao).allVotesFinished(sender), "Staking: votes not finished");

        claim();
        
        uint256 amount = stakeHolder.amount;
        stakeHolder.amount = 0;

        IERC20(lpToken).transfer(sender, amount);

        emit Unstaked(sender, amount, uint64(block.timestamp));
    }

    function setLockTime(uint64 newLockTime) external virtual override onlyDAO {
        
        lockTime = newLockTime;

        emit LockTimeUpdated(lockTime);
    }

}