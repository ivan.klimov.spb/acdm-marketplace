// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;

interface IStaking {
    event Staked(address stakeHoldersAddress, uint256 amount, uint64 timeStartStake);
    event Unstaked(address stakeHoldersAddress, uint256 amount, uint64 unstakeTime);
    event Claimed(address stakeHoldersAddress, uint256 amount);

    event RewardCircleTimerUpdated(uint256 amount);
    event LockTimeUpdated(uint64 amount);
    event RewardPercentageUpdated(uint8 amount);

    function getLockedTokenAmount(address accountAddress) external view returns(uint128);
    function getCurrentSaveReward(address accountAddress) external view returns(uint64);
    function getTimeStartStake(address accountAddress) external view returns(uint64);
    
    function setDAO(address newDAOAddress) external;

    function stake(uint128 amount) external;
    function claim() external;
    function unstake() external;
    function setLockTime(uint64 newLockTime) external;
}